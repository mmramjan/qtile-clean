# Qtile Clean

A clean Qtile installation using only arch base-devel from the EndeavourOS iso.
No desktop environment installed.
pacmanapps is a simple script that installs all the applications needed from the official repos, it needs to be run first after the base arch install.
aurapps is a simple script that installs additional applications from the arch user repo such as extra fonts and other utilities not found in the main arch repo.
